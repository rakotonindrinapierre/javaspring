package com.servicesImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.UserDao;
import com.entities.Users;
import com.services.UserServices;
@Service
public class UserServicesImpl implements UserServices {

	@Autowired
	UserDao userdao;
	
	@Override
	public List<Users> list() {
		// TODO Auto-generated method stub
		return userdao.list();
	}

	@Override
	public boolean delete(Users users) {
		// TODO Auto-generated method stub
		return userdao.delete(users);
	}

	@Override
	public boolean saveOrUpdate(Users users) {
		// TODO Auto-generated method stub
		return userdao.saveOrUpdate(users);
	}

}
